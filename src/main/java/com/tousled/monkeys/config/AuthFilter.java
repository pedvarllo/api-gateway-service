package com.tousled.monkeys.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;


@Component
public class AuthFilter extends AbstractGatewayFilterFactory<AuthFilter.Config> {

    private static final String BEARER = "Bearer";
    private static final String AUTH_USER_HEADER = "x-auth-user-id";

    private final WebClient.Builder webClientBuilder;

    @Autowired
    public AuthFilter(WebClient.Builder webClientBuilder) {
        super(Config.class);
        this.webClientBuilder = webClientBuilder;
    }

    @Override
    public GatewayFilter apply(Config config) {
        return (exchange, chain) -> {
            String authHeader = exchange.getRequest().getHeaders().get(HttpHeaders.AUTHORIZATION).get(0);
            String[] parts = authHeader.split(" ");

            if (parts.length != 2 || !BEARER.equals(parts[0])) {
                throw new IllegalArgumentException("Token is malformed");
            }

            return exchange.getPrincipal()
                    .map(jwt -> {
                        String token = jwt.getName();
                        exchange.getRequest().mutate().header(AUTH_USER_HEADER, token);
                        return exchange;
                    }).flatMap(chain::filter);

        };

    }

    public static class Config {
    }
}
