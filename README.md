[![GPLv3 license](https://img.shields.io/badge/License-GPLv3-blue.svg)](http://perso.crans.org/besson/LICENSE.html)

# API-Gateway Service #

## Project Description ##

This is the API Gateway microservice which belongs to the monkeys-tousled application.
It manages every incoming request to our application and redirects them to the corresponding
microservice.

## Getting started ##

A Makefile has been given in order to run the application. Running the application is
very easy. All you need to do is run the following command in the root path of the project:

```make run-app```

This command also creates a Keycloak image where the Oauth2 tokens for every user registered
in the application will be created. These tokens are required to use this application.

Make sure you previously run the discovery microservice.
For further information, you can read the README.md of this project.

## How to use this project ##

Every request has to be made to the hosted IP (localhost in dev) to the port 8762.

## Dependencies ##

* Spring framework:
    * Spring Cloud Gateway
    * Spring Security
    * Spring Oauth2 Resource Server


### Authors ###

* **Pedro Varela Llorente**
    * [email](pedvarllo@gmail.com)
    * [LinkedIn](https://www.linkedin.com/in/pedvarllo/)
